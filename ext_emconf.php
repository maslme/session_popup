<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Session Popup',
    'description' => 'Configure a popup that appears once a session. Based on session_popup from webschmiede.at.',
    'category' => 'plugin',
    'author' => 'Markus Slaby',
    'author_email' => 'typo3@markus-slaby-media.de',
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '3.0.0',
    'constraints' => [
        'depends' => [
            'typo3' => '11.4.0-11.9.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
