<?php

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['sessionpopup_sessionpopup'] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    // plugin signature: <extension key without underscores> '_' <plugin name in lowercase>
    'sessionpopup_sessionpopup',
    // Flexform configuration schema file
    'FILE:EXT:session_popup/Configuration/FlexForms/SessionpopupPlugin.xml'
);