console.log("Datei session_popup.js geladen");
$(document).ready(function() {

	console.log("session_popup.js wird ausgeführt");

    var layer = $('#session_popup');
    var inner = $('#session_popup_inner');
	var close = $('#session_popup_close');

    layer.hide().fadeIn(500);

    $(document).mouseup(function(e){
		console.log("Mouse Up Funktion gestartet");
        if(!inner.is(e.target) && inner.has(e.target).length === 0){
            layer.fadeOut(500,function(){layer.remove()});
        }
        if(close.is(e.target)){
			console.log('Close wurde gedrückt');
            layer.fadeOut(500,function(){layer.remove()});
        }
    });

});



